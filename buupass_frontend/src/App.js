import React, { Component } from "react"
import Modal from "./components/Modal"; 
import axios from "axios";
import { Label } from "reactstrap";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usermodel: {
        Id: "",
        Name: "",
        Description: "",
        Role: 1,
        Parent: null
      },
      userrolemodel: {
        Id: "",
        Name: "",
        Description: ""
      },
      userList: [],
      userroleList: [],
      module: ""
    };
  }

  async componentDidMount() {
    try {
      const res = await fetch('http://localhost:8000/api/fetchusers/');
      const userList = await res.json();

      const res_roles = await fetch('http://localhost:8000/api/fetchuserroles/');
      const userroleList = await res_roles.json();

      this.setState({
        userList
      });

      this.setState({
        userroleList
      });
    } catch (e) {
      console.log(e);
    }
  }

  toggle = () => {
    this.setState({ modal: !this.state.modal });
  };

  //Responsible for saving the user
  handleSubmit = item => {
    this.toggle();

    if(this.state.module == "User"){
      if (item.Id) {
        axios
          .put(`http://localhost:8000/api/fetchusers/${item.Id}/`, item)
        return;  
      }
      axios
        .post("http://localhost:8000/api/fetchusers/", item)
    }

    if(this.state.module == "Role"){
      if (item.Id) {
        axios
          .put(`http://localhost:8000/api/fetchuserroles/${item.Id}/`, item)
        return;  
      }
      axios
        .post("http://localhost:8000/api/fetchuserroles/", item)
    }
  };

  createUserRole = () => {
    const item = {Name: "", Description: "" };
    this.setState({ userrolemodel: item, modal: !this.state.modal, module: "Role" });
  };

  createParentUser = () => {
    const item = {Name: "", Description: "", Role: "", Parent: "" };
    this.setState({ usermodel: item, modal: !this.state.modal, module: "User" });
  };

  createSubUser = () => {
    const item = {Name: "", Description: "", Role: "", Parent: "" };
    this.setState({ usermodel: item, modal: !this.state.modal, module: "User" });
  };

  renderTabList = () => {
    return (
      <div className="my-5 tab-list">
      </div>  
    );
  };

  renderAllUserRoles = () => {
    const newuserroles = this.state.userroleList.filter(item => item.Id > 0);

    return newuserroles.map(item => (
      <li key={item.Id} >
        <span >
          {item.Name} 
        </span>
        <span >
          ({item.Description})
        </span>
      </li>
    ));
  };

  renderAllUsers = () => {
    const newusers = this.state.userList.filter(item => item.Id > 0);

    return newusers.map(item => (
      <li key={item.Id} >
        <span >
          {item.Name} 
        </span>
        <span >
          ({item.Description})
        </span>
      </li>
    ));
  };

  renderSubUsers = () => {
    const subusers = this.state.userList.filter(item => item.Parent == null);

    return subusers.map(item => (
      <li key={item.Id} >
        <span >
          {item.Name}
        </span>
      </li>
    ));
  };

  render() {
    return (
      <main className="content">
      <h1 className="text-white text-uppercase text-center my-4">BuuPass App</h1>
      <div className="row">
        <div className="col-md-6 col-sm-10 mx-auto p-0">
          <div className="card p-3">
            <div className="">
              <button onClick={this.createUserRole} className="btn btn-success">Add User Role</button>
            </div>
            <div className="">
              <Label>List Of Roles</Label>
            </div>
            {this.renderTabList()}
            <ul className="list-group list-group-flush">
              {this.renderAllUserRoles()}
            </ul>
          </div>
        </div>
      </div>



      <div className="row">
        <div className="col-md-6 col-sm-10 mx-auto p-0">
          <div className="card p-3">
            <div className="">
              <button onClick={this.createParentUser} className="btn btn-success">Add Parent User</button>
            </div>
            <div className="">
              <Label>List Of USers</Label>
            </div>
            {this.renderTabList()}
            <ul className="list-group list-group-flush">
              {this.renderAllUsers()}
            </ul>
          </div>
        </div>
      </div>

      <div className="row">
        <div className="col-md-6 col-sm-10 mx-auto p-0">
          <div className="card p-3">
            <div className="">
              <button onClick={this.createParentUser} className="btn btn-success">Add Sub User</button>
            </div>
            <div className="">
              <Label>List Of Sub Users</Label>
            </div>
            {this.renderTabList()}
            <ul className="list-group list-group-flush">
              {this.renderSubUsers()}
            </ul>
          </div>
        </div>
      </div>


      {this.state.modal ? (
        <Modal
          usermodel = {this.state.usermodel}
          toggle={this.toggle}
          onSave={this.handleSubmit}
        />
      ): null}
    </main>
    )
  }
}

export default App;