from django.apps import AppConfig


class BuupassAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'buupass_app'
