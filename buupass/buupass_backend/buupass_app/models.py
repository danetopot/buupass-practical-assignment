from django.db import models

# Create your models here.
class UserRole(models.Model):
   Id = models.AutoField(primary_key=True)
   Name = models.CharField(max_length=100)
   Description = models.TextField()

   def _str_(self):
     return self.title

class User(models.Model):
   Id = models.AutoField(primary_key=True)
   Name = models.TextField()
   Description = models.TextField()
   Role = models.ForeignKey(UserRole, on_delete = models.CASCADE)
   Parent = models.ForeignKey('self', on_delete = models.CASCADE, blank=True, null=True)

   def _str_(self):
     return self.title
