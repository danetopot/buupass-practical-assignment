from django.contrib import admin
from django.urls import path,include               
from rest_framework import routers                 
from buupass_app import views  

router = routers.DefaultRouter() 
router.register(r'fetchuserroles', views.UserRoleListingView, 'fetchuserrole')
router.register(r'fetchusers', views.UserListingView, 'fetchuser')
router.register(r'fetchsubusers', views.SubUserListingView, 'fetchsubuser')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls))             

]
