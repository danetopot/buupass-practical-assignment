from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import UserRole, User

class UserAdmin(admin.ModelAdmin):
  fields = ['title', 'description']

  admin.site.register(User)
  admin.site.register(UserRole)
