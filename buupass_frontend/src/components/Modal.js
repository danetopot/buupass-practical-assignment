import React, { Component } from "react";
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Input,
    Label

} from "reactstrap";

export default class CustomModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            usermodel: this.props.usermodel
        };
    }
    
    
    handleChange = e => {
        let { name, value } = e.target;
        if (e.target.type === "checkbox") {
            value = e.target.checked;
        }
        const usermodel = { ...this.state.usermodel, [name]: value };
        this.setState({ usermodel });
    };

    render() {
        const { toggle, onSave } = this.props;
        return (
            <Modal isOpen={true} toggle={toggle}>
                <ModalHeader toggle={toggle}>Create User/SubUser</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name">Name</Label>
                            <Input 
                              type="text"
                              name="Name"  
                              value={this.state.usermodel.name}
                              onChange={this.handleChange}
                              placeholder="Name"
                            />
                        </FormGroup>


                        <FormGroup>
                            <Label for="description">Description</Label>
                            <Input 
                              type="text"
                              name="Description"
                              value={this.state.usermodel.description}
                              onChange={this.handleChange}
                              placeholder="User Description"
                            />
                        </FormGroup>

                        <FormGroup>
                            <Label for="role">Role</Label>
                            <Input 
                              type="text"
                              name="Role"
                              value={this.state.usermodel.role}
                              onChange={this.handleChange}
                              placeholder="User Role"
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="parent">Parent</Label>
                            <Input 
                              type="text"
                              name="Parent"
                              value={this.state.usermodel.parent}
                              onChange={this.handleChange}
                              placeholder="User Parent"
                            />
                        </FormGroup>
                        
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="success" onClick={() => onSave(this.state.usermodel)}>
                        Save
                    </Button>
                </ModalFooter>
            </Modal>
        );
    }
}