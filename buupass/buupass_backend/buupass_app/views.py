
# Create your views here.
from django.shortcuts import render
from .serializers import UserSerializer , UserRoleSerializer
from rest_framework import viewsets      
from .models import UserRole, User                 

class UserRoleListingView(viewsets.ModelViewSet):  
    serializer_class = UserRoleSerializer   
    queryset = UserRole.objects.all()

class UserListingView(viewsets.ModelViewSet):  
    serializer_class = UserSerializer   
    queryset = User.objects.all() 

class SubUserListingView(viewsets.ModelViewSet):  
    serializer_class = UserSerializer   
    queryset = User.objects.filter(Parent = None) 