# Generated by Django 4.0.1 on 2022-01-07 10:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('buupass_app', '0003_alter_user_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='Name',
            field=models.TextField(),
        ),
    ]
