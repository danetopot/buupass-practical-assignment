from rest_framework import serializers
from .models import UserRole, User

class UserRoleSerializer(serializers.ModelSerializer):
    Id = serializers.IntegerField(required=False)
    Name = serializers.CharField()
    Description = serializers.CharField()

    class Meta:
        model = UserRole
        fields = ['Id', 'Name', 'Description']
    def create(self, validated_data):
        return UserRole.objects.create(**validated_data)


class UserSerializer(serializers.ModelSerializer):
    Id = serializers.IntegerField(required=False)
    Name = serializers.CharField()
    Description = serializers.CharField()
    Role = serializers.SlugRelatedField(queryset = UserRole.objects.all() , slug_field ='Id') 
    Parent = serializers.SlugRelatedField(queryset = User.objects.all(), slug_field ='Id')

    class Meta:
        model = User
        fields = ['Id', 'Name', 'Description','Role', 'Parent']
    def create(self, validated_data):
        return User.objects.create(**validated_data)